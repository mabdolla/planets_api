const { Pool } = require('pg');

class DB {

    /**
     *  Instantiate the Pool object.
     * This is our connection to the database. The DB class should handle this.
    */
    constructor() {
        this.config = {
            user: 'forest',
            host: 'localhost',
            database: 'planets',
            password: '10111',
            port: 5432,
        };
        this.pool = new Pool(this.config);
    }

    // Expose the pool instance to other classes.
    getPool() {
        return this.pool;
    }
}

// Export a new instance of the Database class as db
module.exports.db = new DB();