class Planet {

    // Construct the Planet class and give it a reference to the pool (The connection to the Database.)
    constructor(pool) {
        this.pool = pool;
    }

    // Get a list of planets in the Database.
    getPlanets(req, resp) {

        // If a planet_id has been sent, only return that planet.
        const { planet_id } = req.query;
        if (planet_id !== undefined) {
            this.getPlanet(req, resp);
        }

        this.pool.query('SELECT * FROM planet', (err, results) => {
            if (err) {
                resp.status(500).json(err);
            }

            resp.json({
                rows: results.rowCount,
                data: results.rows
            });
        })
    }

    // Get a single planet based on an id.
    async getPlanet(req, resp) {
        try {
            const { planet_id } = req.query;
            if (!planet_id) {
                return resp.status(401).json({
                    success: false,
                    message: 'No planet id found'
                });
            }
            const result = await this.pool.query('SELECT * FROM planet WHERE id = $1', [planet_id]);

            if (result.rowCount > 0) {
                return resp.status(200).json({
                    planet: result.rows[0]
                });
            } else {

                return resp.status(404).json({
                    success: false,
                    message: 'Planet not found'
                });
            }
        }
        catch (e) {

        }
    }

    // Add a new planet.
    async addPlanet(req, resp) {
        const { planet } = req.body;
        let errors = [];
        if (!planet.name) errors.push('Planet name is required');
        if (!planet.size) errors.push('Planet size is required');
        if (!planet.image_url) errors.push('Planet image url is required');
        if (!planet.date_discoverd) errors.push('Date discovered is required');

        // One of the properties are missing.
        if (errors.length > 0) {
            resp.status(400).json({
                success: false,
                ...errors
            });
        }
        else {
            try {
                // Use asynchronous call to simplify code.
                const result = await this.pool.query('SELECT * FROM planet WHERE name = $1', [planet.name]);
                if (result.rowCount > 0) {
                    return resp.status(400).json({
                        success: false,
                        message: `Planet ${planet.name} has already added`
                    });
                }

                // Insert a new Planet into the database.
                const insertResult = await this.pool.query(`INSERT INTO planet (name, size, date_discovered, image_url) VALUES($1, $2, $3, $4) RETURNING ID`, [planet.name, planet.size, planet.date_discovered, planet.image_url]);
                if (insertResult.rowCount > 0) {
                    resp.status(201).json({
                        success: true,
                        planet: {
                            id: insertResult.rows[0].id,
                            ...planet
                        }
                    });
                } else {
                    resp.status(500).json({
                        success: true,
                        message: 'Could not create a new planet.'
                    });
                }
            }
            catch (e) {
                return resp.status(500).json({
                    error: e.message
                });
            }
        }
    }

    deletePlanet(req, resp) {

        const { planetId } = req.query;

        if (!planetId) {
            resp.status(400).json({ success: false, error: 'Missing planet id' });
        }

        this.pool.query('DELETE FROM planet WHERE id = $1', [planetId], (err, result) => {
            if (err) {
                resp.status(500).json(err);
            } else {
                resp.status(200).json({
                    success: true,
                    message: result.rowCount === 0 ? 'Planet already deleted' : 'Planet successfully deleted',
                });
            }
        });
    }

    async updatePlanet(req, resp) {
        const { planet } = req.body;
        const errors = [];

        if (!planet.id) errors.push('Planet id is required');
        if (!planet.name) errors.push('Planet name is required');
        if (!planet.size) errors.push('Planet size is required');
        if (!planet.image_url) errors.push('Planet image url is required');
        if (!planet.date_discoverd) errors.push('Date discovered is required');

        if (errors.length > 0) {
            resp.status(400).json({
                success: false,
                errors
            });
        } else {

            try {
                // Extracted to variable for easier reading.
                const updateQuery = `
                    UPDATE planet 
                    SET name = $1, size = $2, date_discovered = $3, image_url = $4
                    WHERE id = $5`;

                // update the planet table with new Planet information.
                const updateResult = await this.pool.query(updateQuery, [planet.name, planet.size, planet.date_discoverd, planet.image_url, planet.id]);

                // If there are any rows affected, the rowCount will be more than 0.
                if (updateResult.rowCount > 0) {
                    resp.status(200).json({
                        success: true
                    })
                } else {
                    // The update failed as no rows where affected.
                    resp.status(500).json({
                        success: false,
                        message: 'Record not found. Nothing updated.'
                    })
                }
            } catch (e) {
                resp.status(500).json({
                    success: false,
                    error: e.message
                })
            }
        }
    }
}

// Export Class for outside use.
module.exports.Planet = Planet;