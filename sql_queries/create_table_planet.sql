CREATE TABLE planet (
    ID SERIAL PRIMARY KEY,
    name VARCHAR(80),
    size VARCHAR(40),
    date_discovered VARCHAR(20),
    img_url VARCHAR(128)
);